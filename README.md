# envsubst

[![pipeline status](https://gitlab.com/keithwoelke/envsubst/badges/master/pipeline.svg)](https://gitlab.com/keithwoelke/envsubst/commits/master)

envsubst is specifically designed with envsubst usage in mind, but it has the full GNU gettext utils suite installed.

Example envsubst usage:

    $ export VAR=foo  
    $ echo '$VAR' | envsubst  
    foo

Example with file (given environment variables already exist):

    echo "$(cat config.yaml | envsubst)" > config.yaml

Additional tools installed:

* git
