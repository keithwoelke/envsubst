#!/usr/bin/env bash

command -v docker &> /dev/null || { echo "Install docker to proceed with setup." ; exit 1; }

git config core.hooksPath scripts/githooks
