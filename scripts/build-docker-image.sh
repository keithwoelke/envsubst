#!/usr/bin/env sh

[ -x "$(command -v docker)" ] || { echo "Docker is not installed." ; exit 1; }

: "${DOCKER_IMAGE_NAME?"DOCKER_IMAGE_NAME must be set."}"
: "${DOCKER_IMAGE_TAG?"DOCKER_IMAGE_TAG must be set."}"

TAG_VERSION="${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"
TAG_LATEST="${DOCKER_IMAGE_NAME}:latest"

project_dir="$(cd "${0%/*}/.." && pwd -P)"

docker build -t "${TAG_VERSION}" -t "${TAG_LATEST}" "${project_dir}/build/package"
